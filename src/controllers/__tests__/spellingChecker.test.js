import {
  fileSpellingChecker,
  fileSpellingFixer,
  spellingFixer,
  spellingChecker,
} from '../spellingChecker';

describe('Test spellingChecker.js', () => {
  describe('Tests on spellingChecker function', () => {
    it('should return empty JSON When given correct sentence', () => {
      const param = 'My name is';

      const result = {};
      expect(() => {
        spellingChecker(param).toEqual(result);
      });
    });

    it('should return a JSON with mistakes and solutions', () => {
      const param = 'Hello worl';

      const result = {
        mistakes: ['worl'],
        solutions: ['world'],
      };

      expect(spellingChecker(param)).toEqual(result);
    });

    // it('should return error when dictionnary is non-existent', () => {
    //   const param = 'Any string will do the job';
    //
    //   dictionaries = undefined;
    //   expect(() => {
    //     spellingChecker(param);
    //   }).toThrow('No dictionnary found');
    // });

    it('should return error when the sentence is not only in english', () => {
      const param = 'It is beautiful, bravo !';

      expect(() => {
        spellingChecker(param);
      }).toThrow('Not supported language');
    });
  });

  describe('Tests on spellingFixer function', () => {
    it('should return the same string when no mistakes', () => {
      const param = 'Welcome to the party !';

      expect(() => {
        spellingFixer(param).toEqual(param);
      });
    });

    it('should return the correct string where there are mistakes', () => {
      const param = 'Welcome to te paty !';
      const result = 'Welcome to the party !';

      expect(() => {
        spellingFixer(param).toEqual(result);
      });
    });

    it('should return an error when given a string in another language than the dictionnary', () => {
      const param = 'Welcome à la party !';

      expect(() => {
        spellingFixer(param);
      }).toThrow('Not supported language');
    });
  });

  describe('Tests on fileSpellingChecker function', () => {
    it('should return an empty JSON when given a proper file', () => {
      const filepath = './spellingChecker.test.txt';
      // eslint-disable-next-line no-unused-vars
      const content = 'This is my sentence';
      // TODO write content to test file.
      const result = {};

      expect(() => {
        fileSpellingChecker(filepath, 'english').toEqual(result);
      });
    });

    it('should return an JSON with mistakes and solutions when given a file with mistakes in it', () => {
      const filepath = './spellingChecker.test.txt';
      // eslint-disable-next-line no-unused-vars
      const content = 'Ths is my setence';
      // TODO write content to test file.
      const result = {
        mistakes: ['Ths', 'setence'],
        solutions: ['This', 'sentence'],
      };

      expect(() => {
        fileSpellingChecker(filepath, 'english').toEqual(result);
      });
    });

    it('should return a error when filepath is incorrect', () => {
      const filepath = './spellingChecker.test.txt@@/non_existent/path/to/non_existent/file/.txt';
      // filepath does not exist

      expect(() => {
        fileSpellingChecker(filepath, 'english');
      }).toThrow('File not found');
    });
  });

  describe('Tests on fileSpellingFixer function', () => {
    it('should return the content of the file unchanged when given a proper file', () => {
      const filepath = './spellingChecker.test.txt';
      const content = 'This is my sentence';
      // TODO write content to test file.

      expect(() => {
        fileSpellingFixer(filepath, 'english').toEqual(content);
      });
    });

    it('should return the content of the file modified when given a file with mistakes', () => {
      const filepath = './spellingChecker.test.txt';
      // eslint-disable-next-line no-unused-vars
      const content = 'Ths is my sentnce';
      // TODO write content to test file.
      const result = 'This is my sentence';

      expect(() => {
        fileSpellingFixer(filepath, 'english').toEqual(result);
      });
    });

    it('should return the content of the file unchanged when given a proper file', () => {
      const filepath = './spellingChecker.test.txt@@/non_existent/path/to/non_existent/file/.txt';
      // eslint-disable-next-line no-unused-vars
      const content = 'This is my sentence';
      // TODO write content to test file.

      expect(() => {
        fileSpellingFixer(filepath, 'english');
      }).toThrow('File not found');
    });
  });
});
