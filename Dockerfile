FROM node:latest

WORKDIR /tp

COPY package.json .
COPY yarn.lock .

RUN yarn install

COPY . .

RUN yarn

ENTRYPOINT ["/tp/cli.js"]
