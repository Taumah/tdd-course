Feature: file reader
    As a developer,
    I want to read the content of a file defined by its filepath,
    So that I can execute my code safely,
    And not having to make my own checks,

    Scenario 1: Filepath correct
        Given that I send a filepath to an existing file,
        When I call the file reader function with my filepath,
        Then it should return the entire content of the file.
    
    Scenario 2: Filepath incorrect
        Given that I send a filepath to an non-existent file,
        When I call the file reader function with my filepath,
        Then it should return a "File not found" error.

    Scenario 3: file with wrong extension
        Given that I send a filepath to an existing file
        And this file is not ending with a supported extension (to be defined)
        Then it should return a "File extension not supported" error.