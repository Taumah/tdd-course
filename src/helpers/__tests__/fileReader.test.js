import { fileWordCounter } from '../../controllers/word-counter';

describe('Test fileReader.js', () => {
  describe('Tests on  fileReader function', () => {
    it('should return the content of my file when given a correct filepath', () => {
      const fileContent = 'Contenu du fichier';
      const filePath = 'fileReader.test.txt';

      expect(() => {
        fileWordCounter(filePath).toEqual(fileContent);
      });
    });

    it('should return an error when given a filepath to a non-existent file', () => {
      const filePath = 'path/to/some/non_existent/fileReader.test.txt';

      expect(() => {
        fileWordCounter(filePath);
      }).toThrow('File not found');
    });

    it('should return an error when given a filepath to a file with a wrong extension', () => {
      const filePath = 'fileReader.test.abcdef';

      expect(() => {
        fileWordCounter(filePath);
      }).toThrow('File extension not supported');
    });
  });
});
