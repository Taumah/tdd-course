Feature: Sentence spelling checker.  
    As a cli user,  
    I want to know which words in my sentence are incorrectly written ,
    So that I can later replace them with provided solutions
    And improve my english

    Scenario 1: Proper sentence provided.
        Given that I wrote a proper sentence without spelling errors,
        When I use the spelling checker cli command with my sentence,
        Then it should return an empty JSON.

    Scenario 2: Sentence with incorrect words provided.
        Given that I wrote an sentence comporting spelling errors,
        When I use the spelling checker cli command with my sentence,
        Then it should return a JSON with as many keys as there are spelling errors 

#    Scenario 3: Proper essay but no dictionary.
#        Given that I wrote a proper essay
#        And did not provide dictionary
#        When I use the spelling checker cli command with my essay,
#        Then it should return a "No dictionary found" error.

    Scenario 4: Not exclusively english sentence.
        Given that I wrote a sentence (containing spelling errors or not)
        And I added some french words like proper nouns,
        When I use the spelling checker cli command with my essay,
        Then it should return a "Not supported language" error.


#################################################################################

Feature: Sentence spelling fixer.  
    As a cli user,  
    I want to correct my sentence when there are spelling errors,
    so that I can send a better sentence

    Scenario 1: Proper sentence provided.
        Given that I wrote a proper sentence without spelling errors,
        When I use the spelling checker cli command with my sentence,
        Then it should return the same sentence as provided.

    Scenario 2: Sentence with incorrect words provided.
        Given that I wrote an sentence comporting spelling errors,
        When I use the spelling checker cli command with my sentence,
        Then it should return sentence with correct words replaced with solutions given by the SpellingChecker function.

    Scenario 3: Sentence with dictionary and language parameter different.
        Given that I wrote an sentence in a language and my dictionary is in another
        When I use the spelling checker cli command with my sentence,
        Then it should return a "Not supported language" error.

#################################################################################

Feature: File Spelling Checker.
    As a cli user,
    I want to know if the content of my file is correctly written,
    So that I can know where my errors are

    Scenario 1: Correct filepath provided and proper essay
        Given that I wrote a proper essay without spelling errors
        And that I sent a correct filepath,
        When I use the file spelling checker cli command,
        Then it should return an empty JSON.

    Scenario 2: Correct filepath provided but mistakes in essay
        Given that I wrote a proper essay with spelling errors,
        When I use the file spelling checker cli command,
        Then it should return a JSON with mistakes and solutions.

    Scenario 3: Incorrect filepath but proper essay
        Given that I wrote a proper essay without spelling errors
        And that I did not send a correct filepath,
        When I use the file spelling checker cli command,
        Then it should return a 'File not found' error.

####################################################################################

Feature: File Spelling Fixer
    As a cli user,
    I want to fix the spelling mistakes in my file,
    So that I do not waste time fixing spelling mistakes one by one by hand

    Scenario 1: Correct filepath and proper essay
        Given that I wrote an essay without spelling mistakes
        And that I sent a correct filepath,
        When I use the file spelling fixer cli command,
        Then it should return the content of my file, unchanged.

    Scenario 2: Correct filepath but mistakes in essay
        Given that I wrote an essay with spelling mistakes,
        When I use the file spelling fixer cli command,
        Then it should return the content of my file without mistakes.

    Scenario 3: Incorrect filepath but proper essay
        Given that I wrote a proper essay without spelling errors
        And that I did not send a correct filepath,
        When I use the file spelling checker cli command,
        Then it should return a 'File not found' error.
