Feature: Sentence Word Counter.  
    As a cli user,  
    I want to count the number of words of a specific sentence,  
    so that I can easily know if my English essay have the good number of words.  

    Scenario 1: Proper essay provided.  
        Given that I wrote a proper sentence of 10 words with no extra or illegal spaces  
        When I use the word counter cli command with my sentence,
        Then I should see that my sentence is 10 words-long.

    Scenario 2: Essay with extra spaces provided.  
        Given that I wrote a proper sentence of 10 words and accidentally added illegal spaces  
        And I accidentally add extra spaces at the beginning and the end of my string
        When I use the word counter cli command with my sentence,
        Then I should see that my sentence is 10 words-long.

###############################################################################################

Feature: File Word counter.
    As a cli user,
    I want to count the number of words of a specific file,
    So that I can make sure my essay has enough words.

    Scenario 1: Filepath correct
        Given that I wrote a proper file of 100 words
        And send the right filepath,
        When I use the word counter cli command with my file,  
        Then I should see that my file is 100 words-long.

    Scenario 2: Filepath incorrect
        Given that I did not send a correct filepath,
        When I use the word counter cli command with my file,  
        Then I should see a 'File not found' error.

